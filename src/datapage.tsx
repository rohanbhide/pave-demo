import { Radio, RadioChangeEvent, Select, Table } from 'antd';
import { autoType, csvParse } from 'd3';

import { Bar } from '@ant-design/charts';
import { EmployeeRow } from './data/EmployeeRow';
import Radium from 'radium';
import React from 'react';
import alasql from 'alasql';

type Props = {
  file: 'gamine' | 'hookfish';
};

type Mode = 'groups' | 'barChart' | 'all';

type Groupable = 'employmentType' | 'level' | 'city' | 'country' | 'department' | 'gender';

type State = {
  barX: Groupable | null;
  groupedBy: Groupable[];
  mode: Mode | null;
  data: EmployeeRow[] | null;
};

const styles: Radium.StyleRules = {
  radio: {
    width: '100%',
    background: 'white',
    boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2)',
    paddingTop: '10px'
  },
  mainDiv: {
    display: 'flex',
    flexDirection: 'column'
  },
  select: {
    width: "50%"
  }
};


export class DataPage extends React.Component<Props, State> {
  state: State = {
    barX: null,
    groupedBy: [],
    mode: null,
    data: null,
  };

  componentWillReceiveProps(nextProps: Props) {
    if (nextProps.file !== this.props.file) {
      this.setState({
        barX: null,
        groupedBy: [],
        mode: null,
        data: null,
      });
    }
  }

  readonly SALARY_COLUMN = { title: 'Salary', dataIndex: 'salary', sorter: this._getComparer('salary') };
  readonly BONUS_COLUMN = { title: 'Bonus', dataIndex: 'bonus', sorter: this._getComparer('bonus') };

  readonly TABLE_COLUMNS = [
    { title: 'First Name', dataIndex: 'firstName', sorter: this._getComparer('firstName') },
    { title: 'Last Name', dataIndex: 'lastName', sorter: this._getComparer('lastName') },
    {
      title: 'Employment Type',
      dataIndex: 'employmentType',
      sorter: this._getComparer('employmentType'),
    },
    { title: 'Level', dataIndex: 'level', sorter: this._getComparer('level') },
    { title: 'City', dataIndex: 'city', sorter: this._getComparer('city') },
    { title: 'Country', dataIndex: 'country', sorter: this._getComparer('country') },
    { title: 'Department', dataIndex: 'department', sorter: this._getComparer('department') },
    { title: 'Gender', dataIndex: 'gender', sorter: this._getComparer('gender') },
    this.SALARY_COLUMN,
    this.BONUS_COLUMN
  ];

  _getComparer(property: keyof EmployeeRow) {
    return (a: EmployeeRow, b: EmployeeRow) =>
      a[property].toLocaleString().localeCompare(b[property].toLocaleString());
  }

  _getAllDisplay(data: EmployeeRow[]) {
    return <Table columns={this.TABLE_COLUMNS} dataSource={data} />;
  }

  _getBarDisplay(data: EmployeeRow[]) {
    let { barX } = this.state;
    let barChart = null;
    if (barX != null) {
      const sql = `SELECT AVG(salary) as salary, AVG(bonus) as bonus, ${barX} FROM ? GROUP BY ${barX} ORDER BY ${barX}`;
      const result = alasql(sql, [data]);
      // Deep clone
      const salaries = JSON.parse(JSON.stringify(result)).map((row: any) => {
        const salary = row['salary'];
        delete row['salary'];
        row['type'] = 'salary';
        row['value'] = salary;
        return row;
      });
      const bonuses = [...result].map((row: any) => {
        const bonus = row['bonus'];
        delete row['bonus'];
        row['type'] = 'bonus';
        row['value'] = bonus;
        return row;
      });
      const config = {
        data: [...salaries, ...bonuses],
        isGroup: true,
        xField: 'value',
        yField: barX,
        seriesField: 'type',
        marginRatio: 0,
        label: {
          layout: [
            { type: 'interval-adjust-position' },
            { type: 'interval-hide-overlap' },
            { type: 'adjust-color' },
          ],
        },
      };
      barChart = <Bar {...config} />;
    }
    return (
      <div>
        <div style={styles.radio}>
          <Radio.Group value={this.state.barX} onChange={this._setBarGroupable}>
            <Radio value={'employmentType'}> Employment Type</Radio>
            <Radio value={'city'}>City</Radio>
            <Radio value={'country'}>Country</Radio>
            <Radio value={'gender'}>Gender</Radio>
            <Radio value={'level'}>Level</Radio>
            <Radio value={'department'}>Department</Radio>
          </Radio.Group>
        </div>
        <div>{barChart}</div>
      </div>
    );
  }

  _getGroupableDisplay(data: EmployeeRow[]) {
    let { groupedBy } = this.state;
    let display = null;
    if (groupedBy.length !== 0) {
      const groupedByString = groupedBy.join(',');
      const sql = `SELECT AVG(salary) as salary, AVG(bonus) as bonus, ${groupedByString} FROM ? GROUP BY ${groupedByString}`;
      const result = alasql(sql, [data]);
      const columns = [];
      for (const groupable of groupedBy) {
        const matching = this.TABLE_COLUMNS.find((column) => column.dataIndex === groupable);
        if (matching != null) {
          columns.push(matching);
        }
      }
      columns.push(this.SALARY_COLUMN, this.BONUS_COLUMN);
      display = <Table columns={columns} dataSource={result} />;
    }
    return (
      <div>
        <div style={styles.radio}>
          <Select mode="multiple" placeholder="Please select some dimensions" onChange={this._setGroupables} style={styles.select}>
          <Radio value={'employmentType'}> Employment Type</Radio>
            <Select.Option value={'city'}>City</Select.Option>
            <Select.Option value={'country'}>Country</Select.Option>
            <Select.Option value={'gender'}>Gender</Select.Option>
            <Select.Option value={'level'}>Level</Select.Option>
            <Select.Option value={'department'}>Department</Select.Option>
          </Select>
        </div>
        <div>{display}</div>
      </div>
    );
  }

  _setBarGroupable = (e: RadioChangeEvent) => {
    this.setState({ barX: e.target.value as Groupable });
  };

  _setMode = (e: RadioChangeEvent) => {
    this.setState({ mode: e.target.value as Mode });
  };

  _setGroupables = (e: any) => {
    this.setState({ groupedBy: e });
  };

  renderDisplay() {
    let display = null;
    const { data, mode } = this.state;
    if (data == null) {
      display = 'Loading...';
    } else {
      if (mode === 'all') {
        display = this._getAllDisplay(data);
      } else if (mode === 'barChart') {
        display = this._getBarDisplay(data);
      } else if (mode === 'groups') {
        display = this._getGroupableDisplay(data);
      }
    }
    return (
      <div style={styles.mainDiv}>
        <div style={styles.radio}>
          <Radio.Group value={this.state.mode} onChange={this._setMode}>
            <Radio value={'all'}> See All Data</Radio>
            <Radio value={'groups'}>Slice Comp. data by dimensions</Radio>
            <Radio value={'barChart'}> Deep Dive into a single dimension</Radio>
          </Radio.Group>
        </div>
        {display}
      </div>
    );
  }

  render() {
    if (this.state.data == null) {
      let comp = this;
      fetch(`/data/${this.props.file}.csv`).then(function (response) {
        if (response.body == null) {
          return;
        }
        let reader = response.body.getReader();
        let decoder = new TextDecoder('utf-8');

        return reader.read().then(function (result) {
          const csvAsString = decoder.decode(result.value);
          const transformed = csvParse(csvAsString, autoType).map((row) => row as EmployeeRow);
          comp.setState({ data: transformed });
        });
      });
    }
    return this.renderDisplay();
  }
}
