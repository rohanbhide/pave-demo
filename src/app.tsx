import './app.css';
import 'antd/dist/antd.css';

import { Content, Header } from 'antd/lib/layout/layout';
import { Layout, Menu } from 'antd';
import React, { useState } from 'react';

import { DataPage } from './datapage';
import PaveLogo from './assets/pave_logo.svg'
import Radium from 'radium';
import SubMenu from 'antd/lib/menu/SubMenu';

const styles: Radium.StyleRules = {
  header: {
    background: "#a9a9a9",
    height: "6vh",
    boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2)"
  },
  logo: {
    height: "3vh",
    marginLeft: ".1vh",
    marginInlineStart: "2vh"
  },
  sider: {
    width: "20%",
    height: "94vh",
  },
  occupyParent: {
    width: "100%",
    height: "100%"
  },
  content: {
    width: "90%",
    height: "94vh",
    display: "flex"
  },
  datapage: {
    width: '80%',
    height: "100%",
    alignContent: "center"
  }
}

function App() {
  const [file, setFile] = useState('');

  let datapage = null;
  if (file === 'gamine' || file === 'hookfish') {
    datapage = <DataPage file={file}/>;
  }
  return (
    <Layout>
      <Header className="header" style={styles.header}>
        <img src={PaveLogo} alt="Logo" style={styles.logo}/>
      </Header>
      <Content style={styles.content}>
        <div style={styles.sider}>
            <Menu mode={"inline"} style={styles.occupyParent} onClick={(e) => setFile(e.key.toString().replace('.$', ''))}>
              <SubMenu key="sub1" title="Restaurants" >
              <Menu.Item key="hookfish">
                Hookfish
              </Menu.Item>
              <Menu.Item key="gamine">
                  Gamine
              </Menu.Item>
              </SubMenu>
          </Menu>
        </div>
        <div style={styles.datapage}>
          {datapage}
        </div>
      </Content>
    </Layout>
  );
}

export default Radium(App);
