export interface EmployeeRow {
  firstName: string,
  lastName: string,
  startDate: Date,
  employmentType: string,
  department: string,
  level: number,
  city: string,
  country: string,
  gender: string,
  salary: number,
  bonus: number,
}